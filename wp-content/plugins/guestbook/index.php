<?php
/*
Plugin Name: Digital Guestbook
Plugin URI: http://www.lywam.org/
Description: TODO
Version: 1.0
Author: Leigh Yawkey Woodson Art Museum
*/

/* Register the Action Hooks with Wordpress */
add_action('admin_head', 'wphook_dashboard_head');
add_action('admin_menu', 'wphook_dashboard_menu');

/* Adds the required stylesheets and script files to the header. */
function wphook_dashboard_head() {
	include("header.php");
}

/* Adds the menu items to the dashboard. */
function wphook_dashboard_menu() {
	add_menu_page("Guestbook", "Guestbook", "edit_pages", "guestbook", 'wphook_page_home');
	add_submenu_page("guestbook", "Collected Data", "Collected Data", "edit_pages", "guestbook-collected-data", "wphook_page_collected_data"); //Page is used to display a quick data report. This may no longer be needed, if a link can be created to a report page.
	add_submenu_page("guestbook", "Questions", "Questions", "edit_pages", "guestbook-questions", "wphook_page_questions"); //Page is used to display a configuration page for questions.
	add_submenu_page("guestbook", "Guests", "Guests", "edit_pages", "guestbook-guests", "wphook_page_guests"); //Page is used to show a sorted list of Guestbook entries.
	add_submenu_page(NULL, "Guest", "Guest", "edit_pages", "guestbook-guest", "wphook_page_guest"); //Page is used to display information about a specific entry. The parent page slug is set to NULL to hide the page from the navigation tree.
	add_submenu_page(NULL, "Report", "Report", "edit_pages", "guestbook-report", "wphook_page_report"); //Page is used to display a report. The parent page slug is set to NULL to hide the page from the navigation tree.
}

/* Returns the directory of the Wordpress plugin installation. */
function dgb_plugin_directory() {
	return plugin_dir_url(__FILE__);
}

/* Reads and decodes a JSON file in a manner which is compatible with WordPress. */
function dgb_decode_json($path) {
	$curl_session = curl_init($path);
	curl_setopt($curl_session, CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($curl_session, CURLOPT_RETURNTRANSFER, true);
	$json = json_decode(curl_exec($curl_session), true);
	curl_close($curl_session);
	return $json;
}

/* Checks if a date string is within a specified range. Takes in a start date, and end date, and the date to check. */
function dgb_date_within_range($a, $b, $c)
{
	$sa = new DateTime($a);
	$sb = new DateTime($b);
	$sc = new DateTime($c);
	return $sa <= $sc && $sc <= $sb;
}

/* Generates the arrays for a report. Takes in the question data, our collected results array, start date, and finish date. */
function dgb_report($data_questions, $data_recent_results, $data_date_start, $data_date_end, $disable_check) {
	$totals_zip_code = array();
    $totals_multiple_choice = array();
    $totals_paragraph = array();
    if(isset($data_date_start) && isset($data_date_end)) {
   		for ($index_entry = 0; $index_entry < count($data_recent_results); $index_entry++) {
        	$entry = $data_recent_results[$index_entry]; 
			if(dgb_date_within_range($data_date_start, $data_date_end, $entry["date"])) { //Ensure that the entry fits within the specified date range. 
				foreach(array_keys($entry) as $param) {
 					if($param == "uniq" || $param == "date") {
	                	continue; //Ignore the UUID and date data components. We do not need them displayed.
	            	}
					for($index_entry_question = 0; $index_entry_question < count($data_questions); $index_entry_question++) {
						$question = $data_questions[$index_entry_question];
						$question_id = $question["ID"];
						$question_type = $question["QuestionType"];
						$question_type_options = $question["QuestionTypeOptions"];
						$question_title = $question["QuestionTitle"];
						$question_enabled = $_GET[$question_id] == "on" || $disable_check;
						$question_enabled_array = $_GET["Radio".$question_id];
						if($question_id == $param) {
							if($question_type == "ZipCode" && $question_enabled) {
								$zip_code = $entry[$param];
								if(!isset($totals_zip_code[$zip_code])) {
        							$totals_zip_code[$zip_code] = 0;
	        					}
	            				$totals_zip_code[$zip_code] += $entry[5]; //TODO: Can we actually parse this out?
	                    	}
		                    if($question_type == "Paragraph" && $question_enabled) {
		                        $answer = $entry[$param];
		                        if(!empty($answer)) {
		                       		if(!isset($totals_paragraph[$question_title])) {
		                            	$totals_paragraph[$question_title] = array();
		                        	}
		                        	array_push($totals_paragraph[$question_title], $answer);
		                        } 
		                    }
		                    if($question_type == "Radio") {
		                        $answer = $entry[$param];
		                        if ($disable_check || (isset($question_enabled_array) && in_array($question_type_options[$answer], $question_enabled_array))) {
		                        	if(!isset($totals_multiple_choice[$question_title][$question_type_options[$answer]])) {
		                            	$totals_multiple_choice[$question_title][$question_type_options[$answer]] = 0;
		                        	}
		                        	$totals_multiple_choice[$question_title][$question_type_options[$answer]] += 1;
		                        }
		                    }
	                	}
	            	}
	        	}
        	}
    	}
    }
    return array($totals_zip_code, $totals_multiple_choice, $totals_paragraph);
}

function dgb_report_guest($data_recent_results, $uuid) {
	for ($i = 0; $i < count($data_recent_results); $i++) {
		if($data_recent_results[$i]["uniq"] == $uuid) {
			return $data_recent_results[$i];
		}
	}
	return NULL;
}

/* Used to dump the results array with a simplified format. TODO: Clean me up */
function dgb_results_dump($array, $level = 0) {
    foreach($array as $key => $value) {
        if(is_array($value)) {
        	echo "<h3>".$key."</h3>";
        	dgb_results_dump($value, $level + 1);
        } else {
        	if(is_int($key)) {
        		echo "<p>".($key + 1).": ".$value."</p>"; //Show the correct numerical index by adding one.
        	} else {
        		echo "<p>".$key.": ".$value."</p>";
        	}
        }
    }
}

/* Compares two results based on their date and returns the most recent. */
function dgb_results_compare($a, $b) {
    $t1 = strtotime($a['date']);
    $t2 = strtotime($b['date']);
    return $t1 - $t2;
}

/* Returns a set of the most recent list items. Takes an array of Guestbook results and a count of items to return. */
function dgb_results_recent_entries($data, $count) {
	$list = (string) NULL;
	usort($data, 'dgb_results_compare');
	$data = array_slice($data, -$count);
	for ($i = 0; $i < $count; $i++) {
		$completed = date('l F jS, Y \a\t h:i A', strtotime($data[$i]["date"])); //Ex: Thursday March 3rd, 2016 at 04:27 PM. TODO: Clean me up
		$list = $list . '<li class="list-group-item"><a href="?page=guestbook-guest&uuid='.$data[$i]["uniq"].'">'.$completed.'</a></li>'; //TODO: Clean me up
	}
	return $list;
}

/* Creates a question and answer table for a single guest entry. Takes an array of question data and an entry object. TODO: Clean me up */
function dgb_results_qa_table($data_questions, $entry) {
	foreach(array_keys($entry) as $param) {
		if($param == "uniq" || $param == "date") {
			continue; //Ignore the UUID and date data components. We do not need them displayed on the table.
		}
		for($i = 0; $i < count($data_questions); $i++) {
			$question = $data_questions[$i];
			if($question["ID"] == $param) {
				echo '<tr>';
				echo '<td>'.$question["QuestionTitle"].'</td>';
				if($question["QuestionType"] == "Radio") {
					echo '<td>'.$question["QuestionTypeOptions"][$entry[$param]].' </td>';
				} else {
					if(empty($entry[$param])) {
						echo '<td>Not Provided</td>'; //Display some placeholder text if no answer was provided.
					} 
					else {
						echo '<td>'.$entry[$param].' </td>';
					}	
				}
				echo '</tr>';
			}
		}
	}
}

/* TODO: Document and clean me up */
function dgb_report_creation_form($data_questions) {
	for($i = 1; $i < count($data_questions) - 1; $i++) {
    	if($data_questions[$i]["QuestionType"] == "Radio") {
    		echo "<span>";
    		echo $data_questions[$i]["QuestionTitle"];
    		echo "</span><br />";
           	for($j = 0; $j < count($data_questions[$i]["QuestionTypeOptions"]); $j++) {
            	echo '<input type="checkbox" value="'.$data_questions[$i]["QuestionTypeOptions"][$j].'" name="Radio'.$data_questions[$i]["ID"].'[]">&nbsp;'.$data_questions[$i]["QuestionTypeOptions"][$j].'</input><br />';
           	}
            echo '<br />';
       	}
       	else {
        	echo '<input type="checkbox" name="'.$data_questions[$i]["ID"].'">&nbsp;'.$data_questions[$i]["QuestionTitle"].'</input><br />';
           	if($data_questions[$i + 1]["QuestionType"] == "Radio") {
           		echo '<br />';
            }
        }
    }
}

function wphook_page_home() {
	$data_questions = dgb_decode_json(dgb_plugin_directory().'question_set.json'); //This must be initialized here because any variable declared outside of this scope is not guaranteed to exist.
	$data_recent_results = dgb_decode_json(dgb_plugin_directory().'results.json'); //This must be initialized here because any variable declared outside of this scope is not guaranteed to exist.
	include("navigation.php");
	include("guestbook.php");
}

function wphook_page_collected_data() {
	$data_questions = dgb_decode_json(dgb_plugin_directory().'question_set.json'); //This must be initialized here because any variable declared outside of this scope is not guaranteed to exist.
	$data_recent_results = dgb_decode_json(dgb_plugin_directory().'results.json'); //This must be initialized here because any variable declared outside of this scope is not guaranteed to exist.
   	$data_date_start = "1970-1-1 00:00:00"; //Unix Epoch
    $data_date_end = date('Y-m-d H:i:s'); //Now
    $report = dgb_report($data_questions, $data_recent_results, $data_date_start, $data_date_end, true);
	include("navigation.php");
	include("report.php");
}

function wphook_page_report() {
	$data_questions = dgb_decode_json(dgb_plugin_directory().'question_set.json'); //This must be initialized here because any variable declared outside of this scope is not guaranteed to exist.
	$data_recent_results = dgb_decode_json(dgb_plugin_directory().'results.json'); //This must be initialized here because any variable declared outside of this scope is not guaranteed to exist.
   	$data_date_start = $_GET["start"];
    $data_date_end = $_GET["end"];
    $report = dgb_report($data_questions, $data_recent_results, $data_date_start, $data_date_end, false);
	include("navigation.php");
	include("report.php");
}

function wphook_page_questions() {
	$data_questions = dgb_decode_json(dgb_plugin_directory().'question_set.json'); //This must be initialized here because any variable declared outside of this scope is not guaranteed to exist.
	include("navigation.php");
	include("questions.php");
}

function wphook_page_guests() {
	$data_questions = dgb_decode_json(dgb_plugin_directory().'question_set.json'); //This must be initialized here because any variable declared outside of this scope is not guaranteed to exist.
	$data_recent_results = dgb_decode_json(dgb_plugin_directory().'results.json'); //This must be initialized here because any variable declared outside of this scope is not guaranteed to exist.
	include("navigation.php");
	include("guests.php");
}

function wphook_page_guest() {
	$data_questions = dgb_decode_json(dgb_plugin_directory().'question_set.json'); //This must be initialized here because any variable declared outside of this scope is not guaranteed to exist.
	$data_recent_results = dgb_decode_json(dgb_plugin_directory().'results.json'); //This must be initialized here because any variable declared outside of this scope is not guaranteed to exist.
	$entry = dgb_report_guest($data_recent_results, $_GET["uuid"]);
	if(empty($entry)) {
		die("Invalid identifier was provided. Please check to make sure it is a valid identifier.");
	} else {
		$completed = date('Y-m-d', strtotime($entry["date"]));
		include("navigation.php");
		include("guest.php");
	}
}

?>