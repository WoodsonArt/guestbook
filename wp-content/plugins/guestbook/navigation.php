<br />
<nav class="navbar navbar-default" role="navigation">
 	<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
      		<span class="sr-only">Toggle Navigation</span>
      		<span class="icon-bar"></span>
      		<span class="icon-bar"></span>
     		<span class="icon-bar"></span>
		</button>
		<a class="navbar-brand" href="?page=guestbook">Guestbook</a>
  	</div>
  	<div id="navbar" class="navbar-collapse collapse">
		<ul class="nav navbar-nav">
			<li><a href="?page=guestbook">Home</a></li>
            <li><a href="?page=guestbook-collected-data">Quick Report</a></li>
			<li><a href="?page=guestbook-questions">Questions</a></li>
			<li><a href="?page=guestbook-guests">Guests</a></li>
		</ul>
		<div class="col-sm-3 col-md-3 pull-right">
        	<form action="admin.php" class="navbar-form" role="search">
        		<div class="input-group">
        			<input type="hidden" name="page" value="guestbook-guest">
            		<input type="text" class="form-control" placeholder="Guest UUID" name="uuid" id="uuid">
            		<div class="input-group-btn">
                		<button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-arrow-right"></i></button>
            		</div>
        		</div>
        	</form>
        </div>
	</div>
 </nav>