<div class="row">
    <div class="col-sm-4">
        <div class="panel panel-default">
            <div class="panel-heading">Recent Guests</div>
            <ul class="list-group">
                <?php echo dgb_results_recent_entries($data_recent_results, 5); ?>
            </ul>
		</div>
	</div>
    <div class="col-sm-8">
        <div class="panel panel-default">
            <div class="panel-heading">Generate Report</div>
			<div class="panel-body">
                <form action="admin.php" class="navbar-form" role="search">
                    <input type="hidden" name="page" value="guestbook-report">
                    <div class="row">
                        <div class="col-md-4">
                            <h4>Select Date Range</h4>
                            <hr />
                            <label for="meeting">Report Start:</label><br /><input id="start" name="start" type="date" value="<?php echo date('Y-m-d')?>"/><br /><br />
                            <label for="meeting">Report End:</label><br /><input id="end" name="end" type="date" value="<?php echo date('Y-m-d')?>"/>
                        </div>
                        <div class="col-md-8">
                            <h4>Select Survey Data</h4>
                            <hr />
                            <?php dgb_report_creation_form($data_questions); ?>
                        </div>
                    </div>
                    <hr />
                    <div class="row">
                        <div class="col-md-12"><button type="submit" class="btn btn-default"><i class="glyphicon glyphicon-download-alt"></i><span>&nbsp;Create Report</span></button></div>
                    </div>
                </form>
			</div>
		</div>
	</div>
</div>