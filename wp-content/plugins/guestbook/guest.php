<div class="row">
	<div class="col-sm-4">
		<div class="panel panel-default">
			<div class="panel-heading">Guest Metadata</div>
			<ul class="list-group">
				<?php
					echo '<li class="list-group-item">Guest ID: '.$entry["uniq"].'</li>';
					echo '<li class="list-group-item">Survey Completed: '.$completed.'</li>';
				?>
			</ul>
		</div>
	</div>
<div class="col-sm-8">
	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>Question</th>
				<th>Answer</th>
			</tr>
		</thead>
		<tbody>
			<?php
				echo dgb_results_qa_table($data_questions, $entry);
			?>
		</tbody>
	</table>
</div>