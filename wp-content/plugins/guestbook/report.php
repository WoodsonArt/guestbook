<div class="row">
    <div class="col-md-12">
        <div class="guestbook-card">
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#statistics" aria-controls="statistics" role="tab" data-toggle="tab">Statistics</a></li>
                <li role="presentation"><a href="#responses" aria-controls="responses" role="tab" data-toggle="tab">Responses</a></li>
                <li role="presentation"><a href="#zips" aria-controls="zips" role="tab" data-toggle="tab">Zip Codes</a></li>
            </ul>
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active padded" id="statistics">
                    <?php
                        echo '<h3>Number of Individual Responses: '.count($data_recent_results).'</h3>';
                    ?>
                </div>
                <div role="tabpanel" class="tab-pane padded" id="responses">
                    <?php 
                        dgb_results_dump($report[1]);
                        dgb_results_dump($report[2]);
                    ?>
                </div>
                <div role="tabpanel" class="tab-pane" id="zips">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Zip Code</th>
                                <th>Total Number of Visitors</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                foreach(array_keys($report[0]) as $paramName) {
                                    echo "<tr><td>".$paramName."</td>";
                                    echo "<td>".$report[0][$paramName]."</td></tr>";
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>